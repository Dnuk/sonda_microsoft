Uproszczona sonda oparta na starej wersji kodu gondoli V2 (odczyt danych GPS na sztywno).

Telemetria tylko przez radiomodem, 
temperatura wewnątrz analogowo na LM335, 
na zewnątrz cyfrowy sensor temperatura i wilgotność - SHT21

Język C, środowisko Atmel Studio 6.x, procesor Atmega644PA.

-- Wszystkie materiały udostępniane są na licencji open software/hardware do użytku niekomercyjnego

-- Uwaga - kompilacja w Atmel Studio 7+ uniemożliwi ponowne przeniesienie projektu do starszej wersji.

Może też wystąpić błąd z funkcją init();. Należy używać jej wtedy jako INLINE.


---------------------------------------------------------------------------------

Schematy, pliki PCB: https://bitbucket.org/Dnuk/gondolav2-hardware-repo/overview

---------------------------------------------------------------------------------
Oprogramowanie Stacji Naziemnej -> StratoTerminal v2.0 [nowy, mniej stabilny] By Fabian Oleksiuk
https://bitbucket.org/FabianoV/stratoterminal2

StratoTerminal v1.1 -> [uzupełnić]


-------------------------------------------------

Kontakt do nas oraz więcej materiałów tutaj: https://www.facebook.com/DNFsystems/
