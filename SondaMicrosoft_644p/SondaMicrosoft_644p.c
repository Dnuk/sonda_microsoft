/*
 * SondaMicrosoft_644p.c
 *
 * Created: 2016-05-18 09:32:00
 *  Author: Dan
 */ 

/*
zdefiniuj F_CPU, popraw czestotliwosci pracy timerow
popraw uarty (uff, japrdle..)

*/


#include <avr/io.h> // standardowe wej/wyj
#include <util/delay.h> // op�nienia
#include <string.h> // operacje na stringach
#include <avr/interrupt.h> // przerwania
#include <stdlib.h> // zawiera funkcje itoa()
#include <avr/wdt.h> // watchdog
#include "MYUART/myuart.h"
#include "TWI/mytwi.h" // I2C
#include <avr/power.h> // makra do oszcz. energii
//#include "math.h"

#define  identyfikator_urzadzenia 'B' // 'A' // 'C' // 'D'
#define  wersja_softu "V28052016"

#define uart0_interrupt_enable UCSR0B |= (1<<RXCIE0)   // radiomodem
#define uart0_interrupt_disable UCSR0B &= ~(1<<RXCIE0)
#define uart0_RX_enable UCSR0B |= (1<<RXEN0)  
#define uart0_RX_disable UCSR0B &= ~(1<<RXEN0)

volatile static uint16_t licznik = 0;
//volatile static uint16_t prog_timer_sek = 0;
volatile static uint8_t prog_timer_sek_1 = 0;
volatile static uint8_t prog_timer_sek_2 = 0;
volatile static uint16_t pseudomillis = 0;
       volatile uint16_t milisek = 0; //////////// zabralem static
volatile static uint16_t delay_timer = 0;
volatile static uint8_t flaga_modem_rx = 0;
volatile static uint8_t flaga_eksperyment_rx = 0;
volatile static uint8_t flaga_radio_rx = 0;
volatile static uint8_t flaga_gps_rx0 = 0; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
volatile static uint8_t flaga_gps_rx1 = 0; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
volatile static uint8_t flaga_gps_rx2 = 0; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

volatile static uint8_t flaga_czysc_buf_exp = 0;

uint16_t modem_sekund = 5;

 char String[83];  // gps
 char String2[45]; // modem
volatile char CzasUTC[7];
volatile char Pozycja[30];
char Pozycja_Decym[20];
volatile char Szerokosc[15]; // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
volatile char Dlugosc[15]; // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
volatile char Wysokosc[8]; // <<<<<<<<<<<<<<<
volatile char LastWysokosc[8]; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
char Wznoszenie[4]; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
char Predkosc[6];
volatile static uint8_t inc = 0;
volatile static uint8_t inc2 = 0;
volatile static char Sat[3];
volatile char Fix[2];

char Temp_wewn_str[6];
char Temp_zewn_str[6];

char bufor[10];
char bufor16[17];

uint16_t LicznikRamek = 1;
volatile static uint8_t enable_morse_flaga = 0;

volatile static char IO_IN_OUT[7]; // {WE1, WY1, WE2, WY2}
	
	char buforTemp[15];
	char buforHumid[15];
	float Temp = 0;
	float Humid = 0;


static inline void _delay_timer_ms(uint16_t czass);
void RESET();
void RESET_Watchdog();
uint16_t adc_pomiar ( uint8_t kanal);
void dane_uart();
static inline void IntToString(int liczba);
//static inline void LongToString(int32_t liczba);
void Konwersja_Pozycji();
void LM335_pomiar();
inline void czysc_buf_wysokosc();
void napiecie_pomiar(); //
void oblicz_Wznoszenie(); // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
void Ogarnij_IO();

void SHT21_Temp_hold();
void SHT21_Humid_hold();

char Napiecie_string[8];

void init()
{
		/*
	MCUSR = 0;						// neutralizuje watchdoga aby po jego uzyciu i resecie nie blokowal procka
	WDTCSR = (1<<WDCE) | (1<<WDE);	// wazne aby to sie wykonalo jak najwczesniej po resecie (tutaj dziala)
	WDTCSR = 0;
	_delay_us(1); */
		
	wdt_enable(WDTO_4S);  // od razu rekonfiguruje watchdoga'a na 4s i go resetuje dla pewnosci
	wdt_reset();

	DDRA = 	 0b11001111;	  // <<ADC>> uzywane kanaly jako wejcia!! - nieuzywane jako wyjscia
	DDRB =   0b11111111;      // 
	DDRC =   0b11101011;      // PC2, PC4 WEJSCIA IO
	DDRD =   0b11111111;       // 

	PORTA &= ~0b11001111; // zera to aktywne kanaly ADC  - reszta podciagana do zera
	PORTB =  0b00000000;    // port B pocz. zera
	PORTC =  0b00000000;    // port C pocz. zera
	PORTD =  0b00000000;    // port D pocz. zera


// inicjalizacja ADC
ADMUX  = (1<<REFS1) | (1<<REFS0);	// Internal 2.56V Voltage Reference
ADCSRA = (1<<ADEN) | (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0);	//enable + konwersja prescaler 128
DIDR0  = 0xFF; // wszystkie adc z odlaczonym rejestrem PIN - oszczedzanie energii i mniejsze zaklocenia

// inicjalizacja UART'u
uart0_init(__UBRR_0);		
uart1_init(__UBRR_1);

I2C_Init(); // I2C initialization


// TIMER2 W TRYBIE CTC
OCR2A = 14;  // 14745600/1024=14400 >> 14400/14=1028,57Hz    
TCCR2A |= (1<<WGM21); //tryb ctc
TCCR2B |= (1<<CS22) | (1<<CS21) | (1<<CS20);    // preskaler TIMER2 1024
TIMSK2 |= (1<<OCIE2A);


sei(); // globalne wlaczenie przerwan (cli() - wy��cza)

_delay_timer_ms(2000);    // guard time dla radiomodemu
uart0_puts(wersja_softu);
uart0_puts(">> INIT OK <<");
}


ISR(USART1_RX_vect)  // OBSLUGA GPS >> TERAZ UART1
{
uart0_interrupt_disable; // blokuje przerwania od portu xbee   <<<<<<<<<<<<<

	unsigned char tmp = UDR1; // przepisz  znak z rejestru do zmiennej (nie, nie da sie bezposrednio)
	if (tmp != 10) { String[inc] = tmp; inc++; } // jesli odebrany znak jest rozny od <LF>=10 lub <CR>=13 to dopisz go do stringa
	else // jesli jednak wystapil taki znak, to znaczy ze skonczyl odbierac stringa
	{

		if ( String[3] == 'G' && String[4] == 'G' && String[5] == 'A' )		// przetwarzanie tych danych jeszcze w przerwaniu by je zabezpieczy� przed nadpisaniem
		{
			for(uint8_t i = 17; i <= 42; i++) Pozycja[i-17] = String[i];
			for(uint8_t i = 54; i <= 60 && String[i] != '.'; i++) Wysokosc[i-54] = String[i];
			for(uint8_t i = 7; i <= 12; i++) CzasUTC[i-7] = String[i];
			
			if (Fix[0] == '2' || Fix[0] == '3') { Sat[0] = String[46]; Sat[1] = String[47]; }
			else { Sat[0] = String[23]; Sat[1] = String[24]; }// 23 i 24 bez fixa
			flaga_gps_rx0 = 0xFF;	
		}

		else if ( String[3] == 'G' && String[4] == 'S' && String[5] == 'A' ) {Fix[0] = String[9]; flaga_gps_rx1 = 0xFF;}
		else if ( String[3] == 'R' && String[4] == 'M' && String[5] == 'C' ) {for(uint8_t i = 46; i <= 50; i++) Predkosc[i-46] = String[i]; flaga_gps_rx2 = 0xFF;}
				
		inc = 0;			//	na koniec zeruj zmienna inkrementujaca tablice stringa (inc)
		//flaga_gps_rx = 1;   // wywal flag� �e ma komplet danych (u�yte w kalkulacji np. pr�dko�ci wznoszenia) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	}													

uart0_interrupt_enable; // odblokowuje przerwania od portu xbee    <<<<<<<<<<<<
}


ISR(USART0_RX_vect)  // odbior komend z ziemii - modem >>> UART0
{
	unsigned char tmp2 = UDR0;
	if (inc2 > 40) {inc2 = 0; flaga_modem_rx = 1;} // gdy przepelni bufor zeruje licznik znakow i przechodzi do analizy bufora (zakonczy sie ona bledem ale wyczysci bufor !!)
	if ( (tmp2 == '>' && inc2 == 0) || (String2[0] == '>' && tmp2 != 'U') )  //  <<<<<<<<<<<<<<<<<<
	{
	if (tmp2 != 13) { String2[inc2] = tmp2; inc2++; } // przepisz znaki do stringa dop�ki nie jest to <CR>
	else flaga_modem_rx = 1;						  // je�li jest, czyli sko�czy� zape�nia� string, to wywal flag�
	}
}


ISR (TIMER2_COMPA_vect) // obsluga przerwania od TIMER2
{
  licznik++;      // zmienna liczaca 1000 przerwan dla 1 sekundy
  milisek++;
  delay_timer++;

  if (licznik >= 1028)// fizyczna liczba przerwan ktore uplyna zanim wykonasz dzialanie  // w tym wypadku ~1s
  	 {
	  	//prog_timer_sek++;
	  	prog_timer_sek_1++;
	  	prog_timer_sek_2++;
	  	licznik = 0;
  	 }
}


int main(void)
{
	init();  // inicjalizacja (wykonuje tylko raz)

	while(1) // GLOWNA PETLA PROGRAMU
	{
		wdt_reset();

		if (flaga_gps_rx0 == 0xFF && flaga_gps_rx1 == 0xFF && flaga_gps_rx2 == 0xFF) // sprawd� czy ma ka�d� ramk� z gps  <<<<<<<<
		{
			flaga_gps_rx0 = 0;		// skasuj flagi od wszystkich ramek
			flaga_gps_rx1 = 0;		//
			flaga_gps_rx2 = 0;		//
			
			if (Wysokosc[0] == ',' && Wysokosc[1] != ',')
			{
				for (int i=0; Wysokosc[i]!=0; i++) // omija blad z przecinkiem na poczatku wysokosci [wycina przecinek] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
				{
					Wysokosc[i]=Wysokosc[i+1];
				}
			}
			oblicz_Wznoszenie(); //						
		}
		

//********************************************************************************************************************** KOMENDY Z ZIEMI
		if (flaga_modem_rx == 1) // wykona to tylko gdy skompletowal komende odebrana z ziemi
	{
			uart0_interrupt_disable;  // blokuje odbieranie komend i innych znakow gdy trwa analiza poprzednich   
			flaga_modem_rx = 0;  // od razu kasuj flage na (wypadek niewykonania dalszej analizy)
			
	   if ( strcmp(String2, ">TXB") == 0 ) // telemetria modem
	   	   {
			LM335_pomiar();
			napiecie_pomiar();
			Konwersja_Pozycji();
			SHT21_Temp_hold(); SHT21_Humid_hold();// TUTAJ POMIAR TEMP I WILGOTNOSCI <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			dane_uart();
			czysc_buf_wysokosc();
	   	   }
			  
	   else if ( strcmp(String2, ">RST") == 0 ) { RESET(); } // reset programu przez skok do bootloadera
		   
	   else if (  strcmp(String2, ">RSTW") == 0  ) { RESET_Watchdog(); } // reset programu przez watchdog
			
		else if ( strcmp(String2, ">WY1=1") == 0 ) // WYJSCIE IO_1 <ON>
		{
			PORTC |= _BV(3);
		}
		
		else if ( strcmp(String2, ">WY1=0") == 0 ) // WYJSCIE IO_1 <OFF>
		{
			PORTC &= ~_BV(3);
		}
		
		else if ( strcmp(String2, ">WY2=1") == 0 ) // WYJSCIE IO_2 <ON>
		{
			PORTC |= _BV(5);
		}
		
		else if ( strcmp(String2, ">WY2=0") == 0 ) // WYJSCIE IO_2 <OFF>
		{
			PORTC &= ~_BV(5);
		}
		
		//else if ( strstr(String2, ">RTR:") != 0 ) // PRZEKAZANIE DANYCH DO PIKACZA   /// gdy bufor zawiera dany ciag znakow
		//{
			//for (int i=0; String2[i]!=0; i++) // przepisuje [przesuwa] string by wywalic tresc komendy a zostawic parametry
			//{
				//String2[i]=String2[i+5];
			//}
			//uart3_puts_noCR(">TX:");
			//uart3_puts(String2);
		//}

	   else uart0_puts("ERROR");


		inc2=0;  // zeruj licznik znakow po wykonaniu komendy

		for(uint8_t i = 0; i<=45; i++)  // czysc bufor komend
					{
						String2[i] = 0;
					}

			uart0_interrupt_enable;	//	odblokowuje odbieranie komend i innych znakow po analizie    
	}
		
//************************************************************************************************************************ KONIEC KOMEND Z ZIEMI


		if (prog_timer_sek_2 >= 1)	// wykonuje sie co 1s
		{
			PORTB ^= _BV(4);		// co 1s neguj portb.4 by zresetowac zewn. watchdog  //////// !!!!!!!!!!!!!!!!!!!!!!!!!
			prog_timer_sek_2 = 0;

			if(Fix[0] == '2' || Fix[0] == '3')	// dodatkowo sprawdz czy jest fix
			 {
				modem_sekund = 2;				// gdy jest fix ustaw czestsze nadawanie
			 }
			else
			{
				modem_sekund = 10;				// przy kazdej innej wartosci fixa ustaw zadsze nadawanie
			}
			
			Ogarnij_IO();						
		}

		if (prog_timer_sek_1 >= modem_sekund)    // TELEMETRIA UART CO ~2s
		    {									// jesli timer programowy zliczyl czas
				LM335_pomiar();
				napiecie_pomiar();
				Konwersja_Pozycji();
				SHT21_Temp_hold(); SHT21_Humid_hold();// TUTAJ POMIAR TEMP I WILGOTNOSCI <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				dane_uart();
				czysc_buf_wysokosc();
		        prog_timer_sek_1 = 0;
		    }


		// koniec while'a /////////////////////////////////////////////////////////////////////////////////// koniec while'a
	}
}

void dane_uart()
{
		uart0_putc('#');
		uart0_putc( identyfikator_urzadzenia ); 
		
		IntToString(LicznikRamek);
		uart0_puts_noCR( bufor ); // bufor licznika ramek
		uart0_putc( ';' ); 
		uart0_puts_noCR( CzasUTC );
		uart0_putc( ';' ); 
		uart0_puts_noCR( Temp_wewn_str );
		uart0_putc( ';' ); 
		uart0_puts_noCR( buforTemp ); //// Temp_zewn_str
		uart0_putc( ';' ); 			
		uart0_puts_noCR( Napiecie_string );
		uart0_putc( ';' ); 
		uart0_puts_noCR( Fix );
	    uart0_putc( ';' );  

		
		if( Sat[0] != 0 ) uart0_puts_noCR( Sat ); //<<<<<<<<<<< byl warning. czepil sie wpisu volatile'a do argumentu non-volatile (zmieniono argument w funkcji, jest ok)
	    else uart0_puts_noCR( "??" );
		uart0_putc( ';' ); 

	    if(Fix[0] == '2' || Fix[0] == '3')
	    {
			if(Wysokosc[0] == 0 && Wysokosc[1] == 0) uart0_puts_noCR( "??" ); // <<<<<<<<<<<<<< ???
			else uart0_puts_noCR( Wysokosc );
			uart0_putc( ';' ); 		
			uart0_puts_noCR( Szerokosc );
			uart0_putc( ';' ); 
			uart0_puts_noCR( Dlugosc );
			uart0_putc( ';' ); 
			uart0_puts_noCR( Predkosc );		
			uart0_putc( ';' ); 
			uart0_puts_noCR( Wznoszenie );
			uart0_putc( ';' ); 
	    }
	    else 
		{
			uart0_puts_noCR( "??;" );
			uart0_puts_noCR( "??;" );
			uart0_puts_noCR( "??;" );
			uart0_puts_noCR( "??;" );
			uart0_puts_noCR( "??;" );
		}
			uart0_puts_noCR( IO_IN_OUT );
			uart0_putc( ';' );
			
			//uart0_puts_noCR("%RH:");
			uart0_puts_noCR( buforHumid ); // <<<<<<<<
			uart0_puts( ";" );		// <<<<<<<<<
			
			LicznikRamek++;
			
}


static inline void _delay_timer_ms(uint16_t czass)
{
	delay_timer = 0;
	while(delay_timer <= czass);
}


void RESET() // funkcja resetowania CPU przez skok do poczatku pamieci bootloadera
{
	cli();				  // deaktywuje przerwania zewnetrzne
	_delay_timer_ms(100); // poczekaj troszke
	asm("jmp 0x7800;");	  // skocz do poczatku pamieci bootloadera (przy 2kb pojemnosci >> 4rech?)
}

void RESET_Watchdog() // <<<<<<<<<<< !!!!!!!!!!!!!!!
{
	   _delay_ms(1000);
	   cli();
	   wdt_enable(WDTO_30MS);
	   while(1);
}

/*     wykonanie pomiaru ADC     */
uint16_t adc_pomiar ( uint8_t kanal) ///////////////////////////// 
{
	ADMUX &=~0x07;	//clear last 3 bits
	ADMUX |= (0x07 & kanal); //select channel
	ADCSRA |=(1<<ADSC);	//start konwersji

	while( !(ADCSRA & (1<<ADIF)) ); //czekaj na koniec konwersji
	//return ADCH;

	unsigned char x = ADCL;
	unsigned char y = ADCH;

	return (y<<8) + x;
}

static inline void IntToString(int liczba)
{
	itoa(liczba, bufor, 10);
}

//static inline void LongToString(int32_t liczba)
//{
	//ltoa(liczba, bufor16, 10);
//}


void Konwersja_Pozycji()	// zamiana pozycji gps z formatu protokolu NMEA na format dziesietny
{	
	char Szer_pocz[4];
	char Dlug_pocz[4];
	volatile static double Szerokosc_liczba = 0;
	volatile static double Dlugosc_liczba = 0;
	volatile static double Szer_liczba_pocz = 0; 
	volatile static double Dlug_liczba_pocz = 0; 
	
	for(uint8_t i = 0; i <= 7; i++) Szerokosc[i] = Pozycja[i+2]; // przepisz z pozycji tylko dalsza czesc szerokosci geo
	for(uint8_t i = 0; i <= 7; i++) Dlugosc[i] = Pozycja[i+16];  // przepisz sama dalsza czesc dlugosci geo
	Szer_pocz[0]=Pozycja[0]; Szer_pocz[1]=Pozycja[1];			// przepisz tylko stopnie szerokosci
	Dlug_pocz[0]=Pozycja[13]; Dlug_pocz[1]=Pozycja[14];	Dlug_pocz[2]=Pozycja[15]; // przepisz tylko stopnie dlugosci

	Szer_liczba_pocz = atof(Szer_pocz);			// konwersja stringow na liczby zmiennoprzecinkowe
	Dlug_liczba_pocz = atof(Dlug_pocz);			//
	Szerokosc_liczba = atof(Szerokosc);			//
	Dlugosc_liczba = atof(Dlugosc);				//

	Szerokosc_liczba = Szerokosc_liczba/60.0 + Szer_liczba_pocz; // przeliczenie na pozycje w formacie decymentalnym
	Dlugosc_liczba = Dlugosc_liczba/60.0 + Dlug_liczba_pocz;

	dtostrf(Szerokosc_liczba,0,5,Szerokosc);		// konwersja spowrotem do stringa
	dtostrf(Dlugosc_liczba,0,5,Dlugosc);			// dla wygody uzyto zmiennych z poczatku funkcji [szerokosc i dlugosc]
}

void napiecie_pomiar()// <<<<<<<<<<<<<< #2
{
	for(uint8_t i = 0; i<=8; i++) // czysci stringa
	{
		Napiecie_string[i] = 0;
	}
	
	volatile static double Napiecie_cyfra = 0; // zeruje zmienna liczbowa
	
	for (int i=0; i<50; i++)
	{
		Napiecie_cyfra = Napiecie_cyfra + adc_pomiar(5);
	}
		Napiecie_cyfra = (Napiecie_cyfra/50)* 0.01051365; // zmienne: pierwsza to mno�nik wynikajacy z dzielnika i konwersji adc, druga to kalibracyjna
				   //  lepszy mnoznnik: 0.01082615 zamiast 0.01134732 - 0.394
/*
ma byc 5.51v
pokazuje 5.67v >>>>> czyli zmniejszy� mno�nik
r�znica 0.160v
(1024 * 0.01082615) - 0.16 = 1024 * x
x = 0.0106699 >>>> ZWERYFIKOWA� NA SPRZ�CIE <<<<<<<<
DLA 512>> X= 0,01051365

NAJLEPIEJ POMIERZY� REZYSTORY I PRZELICZYC NA NOWO
*/
	
	dtostrf(Napiecie_cyfra,0,3,Napiecie_string); 
}

void LM335_pomiar()
{
	volatile static double Temp_wewn = 0;
	//volatile static int Temp_zewn = 0;
	const double mnoznikTemp = 3.24284;
	const uint8_t iloscPomiarow = 100;
	
	for(int i=0; i<iloscPomiarow; i++)
	{
		Temp_wewn = Temp_wewn + adc_pomiar(4); // lm35D wewn >>> LM335 ( skalibruj) by�o 3.33555
	}
		Temp_wewn= Temp_wewn/iloscPomiarow;
		Temp_wewn = ((Temp_wewn * mnoznikTemp / 1024.0 * 100.0) - 273.15 );
		
	//for(int i=0; i<100; i++)
	//{
		//Temp_zewn = Temp_zewn + ((adc_pomiar(14) / 2) -82); // lm35D zewn
	//}
		//Temp_zewn = Temp_zewn/100;

	dtostrf(Temp_wewn,0,0,Temp_wewn_str);//itoa(Temp_wewn, Temp_wewn_str, 10); //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> zmieniono na zero miejsc  po przecinku 
	//itoa(Temp_zewn, Temp_zewn_str, 10);
}

// TU POMIAR WILGOTNOSCI I TEMPERATURY2

inline void czysc_buf_wysokosc()
{
	for(uint8_t i = 0; i<=8; i++)
	{
		Wysokosc[i] = 0;
	}
}

void oblicz_Wznoszenie() // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
{
	int WysokoscInt = atoi(Wysokosc); // skonwertuj string do int'a
	int LastWysokoscInt = atoi(LastWysokosc); // inty pomocnicze do oblicze�
	int WznoszenieInt = 0;
	
	WznoszenieInt = (WysokoscInt - LastWysokoscInt) / 1; // r�nica w wysoko�ci dzielona przez czas mi�dzy pomiarami w sek.
	itoa(WznoszenieInt, Wznoszenie, 10); // konwersja gotowego wznoszenia
	
	for (uint8_t i=0; i<4; i++){ LastWysokosc[i] = Wysokosc[i];} // przepisz obecn� wysoko�� jako poprzedni�
}

void Ogarnij_IO()
{
	if ((PINC & _BV(2)) == _BV(2)) {IO_IN_OUT[0] = '1';} // WE-1
		else {IO_IN_OUT[0] = '0';} 
			
	if ((PINC & _BV(3)) == _BV(3)) {IO_IN_OUT[1] = '1';} // WY-1
		else {IO_IN_OUT[1] = '0';} 
			
	if ((PINC & _BV(4)) == _BV(4)) {IO_IN_OUT[2] = '1';} // WE-2
		else {IO_IN_OUT[2] = '0';} 
			
	if ((PINC & _BV(5)) == _BV(5)) {IO_IN_OUT[3] = '1';} // WY-2
		else {IO_IN_OUT[3] = '0';} 
}


void SHT21_Temp_hold()
{
	I2C_SendStartAndSelect(0b10000000); //  adress + write
	I2C_SendByte(0b11100011); // command: temp, hold master mode
	I2C_SendStartAndSelect(0b10000001); //  adress + read
	_delay_ms(85); // czekanie na pomiar (przy 14bit, min 65ms - max 85ms)
	uint8_t MSB = I2C_ReceiveData_ACK(); // odczyt MFB
	uint8_t LSB = I2C_ReceiveData_NACK(); // odczyt LFB bez sumy kontrolnej
	LSB &= ~( (1<<0) | (1<<1) ); // zeruje dwa najm�odsze bity LSB (stat.) >> 11111100
	uint16_t odczyt = (MSB<<8)|(LSB);
	Temp = -46.85 + 175.72 * odczyt / 65536.0; // Temp - zmienna globalna - jako zmienna
	dtostrf(Temp,0,0,buforTemp);			 // buforTemp - zmienna globalna - jako string //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> zmieniono na zero miejsc  po przecinku
}

void SHT21_Humid_hold()
{
	I2C_SendStartAndSelect(0b10000000); //  adress + write
	I2C_SendByte(0b11100101); // command: Humidity, hold master mode
	I2C_SendStartAndSelect(0b10000001); //  adress + read
	_delay_ms(29); // czekanie na pomiar (przy 12bit, min 22ms - max 29ms)
	uint8_t MSB = I2C_ReceiveData_ACK(); // odczyt MFB
	uint8_t LSB = I2C_ReceiveData_NACK(); // odczyt LFB bez sumy kontrolnej
	LSB &= ~( (1<<0) | (1<<1) ); // zeruje dwa najm�odsze bity LSB (stat.) >> 11111100
	uint16_t odczyt = (MSB<<8)|(LSB);
	Humid = -6.0 + 125.0 * odczyt / 65536.0; // Humid - zmienna globalna - jako zmienna
	dtostrf(Humid,0,2,buforHumid);			 // buforHumid - zmienna globalna - jako string
}
