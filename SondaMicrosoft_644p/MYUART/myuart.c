

#include "avr/io.h"
#include "avr/pgmspace.h"
#include "myuart.h"
#include "stdlib.h"


void uart0_init( uint16_t ubrr)
{
/*Set baud rate */
UBRR0H = (uint8_t)(ubrr>>8);
UBRR0L = (uint8_t)ubrr;
/*Enable transmitter and/or receiver */
UCSR0B = (1<<RXCIE0)|(1<<RXEN0)|(1<<TXEN0); 
}

/*
void uart0_zmien_baud( uint16_t ubrr)
{
//Set baud rate
UBRR0H = (uint8_t)(ubrr>>8);
UBRR0L = (uint8_t)ubrr;
}
*/

void uart0_putc( uint8_t data )
{
// Wait for empty transmit buffer
while( !( UCSR0A & (1<<UDRE0)) );
// Put data into buffer, sends the data
UDR0 = data;
}

void uart0_puts( char * s )   // wysy�anie string'ow
{
	while( *s ) uart0_putc( *s++ );
	uart0_putc( 13 ); // dodano powr�t karetki
}

void uart0_puts_noCR(volatile char * s )   // wysy�anie string'ow // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< zmiana na volatile
{
	while( *s ) uart0_putc( *s++ );
}

/*
void uart_puts_P(const char *s)  // wysy�anie z pami�ci flash
{
	register char c;
	while ((c = pgm_read_byte( s++) )) uart_putc(c);
}
*/

void uart0_putlong( uint32_t liczba, uint8_t radix)  // wysylanie liczb z pamieci
{
	char buf[17];
	ltoa( liczba, buf, radix);
	uart0_puts( buf );
}

/////////////////////////////////////////////////////////////////////////

// DLA UK�ADU Z DWOMA UARTAMI


void uart1_init( uint16_t ubrr)
{
//Set baud rate
UBRR1H = (uint8_t)(ubrr>>8);
UBRR1L = (uint8_t)ubrr;
//Enable transmitter and/or receiver
UCSR1B = (1<<RXCIE1)|(1<<RXEN1);//|(1<<TXEN1); // przerwanie odbioru, odbior, nadawanie [NADAWANIE DO GPS WY��CZONE - KONFLIKT POZIOM�W NAPI��! 4.8V >> 3.3V - BUFOR LUB OBNI�Y� DO 3.3]
}


void uart1_putc( uint8_t data )
{
// Wait for empty transmit buffer
while( !( UCSR1A & (1<<UDRE1)) );
// Put data into buffer, sends the data
UDR1 = data;
}


void uart1_puts( char * s )   // wysy�anie string'ow
{
	while( *s ) uart1_putc( *s++ );
	uart1_putc( 13 );
}

void uart1_puts_noCR(volatile char * s )   // wysy�anie string'ow
{
	while( *s ) uart1_putc( *s++ );
}


void uart1_putlong( uint32_t liczba, uint8_t radix)  // wysylanie liczb z pamieci
{
	char buf[17];
	ltoa( liczba, buf, radix);
	uart1_puts( buf );
}

//////////////////////////////////////////////////////////
////////////////////////////////////////////////////////// cztery uarty

//
//void uart2_init( uint16_t ubrr)
//{
	////Set baud rate
	//UBRR2H = (uint8_t)(ubrr>>8);
	//UBRR2L = (uint8_t)ubrr;
	////Enable transmitter and/or receiver
	//UCSR2B = (1<<RXCIE2)|(1<<RXEN2)|(1<<TXEN2); // przerwanie odbioru, odbior, nadawanie
//}
//
//void uart2_putc( uint8_t data )
//{
	//// Wait for empty transmit buffer
	//while( !( UCSR2A & (1<<UDRE2)) );
	//// Put data into buffer, sends the data
	//UDR2 = data;
//}
//
//void uart2_puts(volatile char * s )   // wysy�anie string'ow
//{
	//while( *s ) uart2_putc( *s++ );
	//uart2_putc( 13 );
//}
//
//void uart2_puts_noCR(volatile char * s )   // wysy�anie string'ow
//{
	//while( *s ) uart2_putc( *s++ );
//}
//
//void uart2_putlong( uint32_t liczba, uint8_t radix)  // wysylanie liczb z pamieci
//{
	//char buf[17];
	//ltoa( liczba, buf, radix);
	//uart2_puts( buf );
//}
//
/////////
//
//void uart3_init( uint16_t ubrr)
//{
	////Set baud rate
	//UBRR3H = (uint8_t)(ubrr>>8);
	//UBRR3L = (uint8_t)ubrr;
	////Enable transmitter and/or receiver
	//UCSR3B = (1<<RXCIE3)|(1<<RXEN3)|(1<<TXEN3); // przerwanie odbioru, odbior, nadawanie
//}
//
//void uart3_putc( uint8_t data )
//{
	//// Wait for empty transmit buffer
	//while( !( UCSR3A & (1<<UDRE3)) );
	//// Put data into buffer, sends the data
	//UDR3 = data;
//}
//
//void uart3_puts(volatile char * s )   // wysy�anie string'ow
//{
	//while( *s ) uart3_putc( *s++ );
	//uart3_putc( 13 );
//}
//
//void uart3_puts_noCR(volatile char * s )   // wysy�anie string'ow
//{
	//while( *s ) uart3_putc( *s++ );
//}
//
//void uart3_putlong( uint32_t liczba, uint8_t radix)  // wysylanie liczb z pamieci
//{
	//char buf[17];
	//ltoa( liczba, buf, radix);
	//uart3_puts( buf );
//}

