

#ifndef MYUART_H_
#define MYUART_H_

#define UART0_BAUD 57600 // MODEM 
#define __UBRR_0 (  F_CPU / 16 / UART0_BAUD - 1   )

#define UART1_BAUD 19200      // GPS
#define __UBRR_1 (  F_CPU / 16 / UART1_BAUD - 1   )

//#define UART2_BAUD 57600 // MODEM
//#define __UBRR_2 (  F_CPU / 16 / UART2_BAUD - 1   )
//
//#define UART3_BAUD 19200      // RADIO
//#define __UBRR_3 (  F_CPU / 16 / UART3_BAUD - 1   )


void uart0_init( uint16_t ubrr);
//void uart_zmien_baud( uint16_t ubrr);
void uart0_putc( uint8_t data );
void uart0_puts( char * s );
void uart0_puts_noCR( volatile char * s );    // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< zmiana na volatile
//void uart_puts_P(const char *s);
void uart0_putlong( uint32_t liczba, uint8_t radix);


//////////////////////// DLA UK�ADU Z DWOMA UARTAMI

void uart1_init( uint16_t ubrr);
//void uart_zmien_baud( uint16_t ubrr);
void uart1_putc( uint8_t data );
void uart1_puts( char * s );
void uart1_puts_noCR( volatile char * s );    // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< zmiana na volatile
//void uart_puts_P(const char *s);
void uart1_putlong( uint32_t liczba, uint8_t radix);


///// cztery uarty
//
//void uart2_init( uint16_t ubrr);
////void uart_zmien_baud( uint16_t ubrr);
//void uart2_putc( uint8_t data );
//void uart2_puts(volatile char * s );
//void uart2_puts_noCR(volatile char * s );
////void uart_puts_P(const char *s);
//void uart2_putlong( uint32_t liczba, uint8_t radix);
//
//
//void uart3_init( uint16_t ubrr);
////void uart_zmien_baud( uint16_t ubrr);
//void uart3_putc( uint8_t data );
//void uart3_puts(volatile char * s );
//void uart3_puts_noCR(volatile char * s );
////void uart_puts_P(const char *s);
//void uart3_putlong( uint32_t liczba, uint8_t radix);



#endif /* MYUART_H_ */
