#include "avr/io.h"
#include "util/twi.h"
#include "mytwi.h"
#include <util/delay.h> // opóźnienia

#define I2CBUSCLOCK 200 // predkosc magistrali I2C
#define I2C_STARTError 1
#define I2C_NoNACK 3
#define I2C_NoACK 4

uint8_t I2C_Error; // zmienna do przechowania kodu bledu

 void I2C_SetError(uint8_t err) //inline
{
	I2C_Error=err;
}

void I2C_SetBusSpeed(uint16_t speed)
{
	speed = (F_CPU/speed/100-16)/2; //speed=TWBR*4^TWPS
	uint8_t prescaler = 0;
	while(speed>255) // oblicz wartosc preskalera
	{
		prescaler++;
		speed=speed/4;
	}
	TWSR=(TWSR & (_BV(TWPS1)|_BV(TWPS0))) | prescaler;
	TWBR=speed;
}

void I2C_SetCalculatedBusSpeed(uint16_t twbr, uint8_t prescal)
{
	TWSR=(TWSR & (_BV(TWPS1)|_BV(TWPS0))) | prescal;
	TWBR=twbr;
}


void I2C_Init()
{
	TWCR = _BV(TWEA) | _BV(TWEN); // wlacz interfejs twi
	//I2C_SetBusSpeed(I2CBUSCLOCK/100);
	I2C_SetCalculatedBusSpeed(0xE4,0x01); //[0xE4/0x01]>>10kHz
}

 void I2C_WaitForComplete() //static inline
{
	uint32_t mikrosek = 0;
	while (!(TWCR & _BV(TWINT)))
	{
		if(mikrosek >= 1500) { break;} // timer do uciekania z funkcji gdy sensor nie odpowiada
		_delay_us(1);
		mikrosek++;
	}
}

void I2C_Start()
{
	TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
	I2C_WaitForComplete();
	if (TW_STATUS != TW_START) I2C_SetError(I2C_STARTError);
}

 void I2C_Stop() //static inline
{
	TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);
}

 void I2C_WaitTillStopWasSent() //static inline
{
	uint32_t mikrosek = 0;
	while (TWCR & _BV(TWSTO));
	{
		//if(mikrosek >= 150) { break;}
		//_delay_us(1);
	}
}

void I2C_SendAddr(uint8_t address)
{
	uint8_t Status;

	if( (address & 0x01)== 0 ) Status = TW_MT_SLA_ACK;
	else Status = TW_MR_SLA_ACK;

	TWDR = address;
	TWCR = _BV(TWINT) | _BV(TWEN);
	I2C_WaitForComplete();
	if(TW_STATUS != Status) I2C_SetError(I2C_NoACK);
}

void I2C_SendStartAndSelect(uint8_t addr)
{
	I2C_Start();
	I2C_SendAddr(addr);
}

void I2C_SendByte(uint8_t byte)
{
	TWDR=byte;
	TWCR = _BV(TWINT) | _BV(TWEN);
	I2C_WaitForComplete();
	if( TW_STATUS!=TW_MR_DATA_ACK) I2C_SetError(I2C_NoACK);
}

uint8_t I2C_ReceiveData_NACK()
{
	TWCR= _BV(TWINT) | _BV(TWEN);
	I2C_WaitForComplete();
	if (TW_STATUS!=TW_MR_DATA_NACK) I2C_SetError(I2C_NoNACK);
	return TWDR;
}

uint8_t I2C_ReceiveData_ACK()
{
	TWCR= _BV(TWEA) | _BV(TWINT) | _BV(TWEN);
	I2C_WaitForComplete();
	if(TW_STATUS!=TW_MR_DATA_ACK) I2C_SetError(I2C_NoACK);
	return TWDR;
}



